Info

Celem projektu jest zrobienie gry bazującej na popularnej grze Breakout/Arkanoid. Za sukces można będzie uznać bezbłędnie działający program w bashu, realizujący grę.

W skład grupy realizującej projekt wchodzą:
	-Bartosz Wodziński
	-Mateusz Wytrwał

Plan Minimum

-Program Działa
	-Działający "paddle"
		-Porusza się po naciśniąciu klawiszy
	-Klocki
		-Po uderzeniu piłką znikają
	-Piłka
		-Porusza się
		-Obsługuje kolizje ze
			-ścianami
			-paddlem
			-klockami
		-Znika po nie trafieniu paddlem
	-Poziomy
		-Jeden poziom
	-Punkty
		-Zniszczenie klocka daje punkty
	-Koniec gry
		-Zniszczenie piłki, Przegrana
		-Zniszczenie wszystkich klocków, Wygrana
	-Zapisywanie Wyników

Dodatkowe Funkcje

-Obsługa wielu poziomów
	-Na przykład 10 poziomów, gra potrafi przechodzić między nimi
	-Przejście wszystkich poziomów odblokowuje jakąś nagrodę
-Piłka
	-Piłka zwiększa prędkość z czasem
	-Kąt odbicia zależy od miejsca kolizji z paddlem
-Power Ups
	-Klocki po rozbiciu mogą randomowo dropnąć power up
	-Power up po zebraniu modyfikuje piłkę, paddla lub daje dodatkowe punkty
-Dźwięki
	-Obsługa dźwięków (O ile w bashu się da)
-Opcja gry na wybranym poziomie

Podział pracy

Pracę będziemy ustalać raczej na bieżąco, podobnie plan może się zmienić. Priorytetem tak czy inaczej jest działanie programu niż poszczególne elementy dodatkowe.

-Po ustaleniu jakiegoś bardziej szczegółowego planu, wrzucimy go tutaj.

Jeśli chodzi o funkcjonalności opcjonalne, nie ma pewności czy w ogóle będą implementowane, i jeśli już to po zrealizowaniu wersji podstawowej.