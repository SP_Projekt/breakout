#!/bin/bash

##########DEKLARACJE ZMIENNYCH I TABLIC#################################

#####Ustawienie wielkości konsoli i "zniknięcie" karetki
printf "\e[?25l";
printf "\e[8;41;76t";
sleep 0.1;
printf "\e[8;41;76t";
#####Właściwe deklaracje
declare -i liczbaWierszy=$(($(tput lines) - 1));
declare -i liczbaKolumn=$(tput cols);
declare -i szerokoscPaddlea=10;
declare zoltyBlok="\e[48;5;11m \e[0m";
declare jasnoZoltyBlok="\e[48;5;226;11m \e[0m";
declare czerwonyBlok="\e[48;5;9m \e[0m";
declare jasnoNiebieskiBlok="\e[106m \e[0m";
declare niebieskiBlok="\e[48;5;39m \e[0m";
declare zielonyBlok="\e[42m \e[0m";
declare bialyBlok="\e[107m \e[0m";
declare szaryBlok="\e[47m \e[0m";
declare pomaranczowyBlok="\e[48;5;214m \e[0m";
declare fioletowyBlok="\e[48;5;5m \e[0m";
declare bordowyBlok="\e[48;5;52m \e[0m";
declare niebieskoFioletowyBlok="\e[48;5;12m \e[0m";
declare czarnyBlok="\e[48;5;0m \e[0m";
declare kolorBloku1=$bordowyBlok;
declare kolorBloku2=$zielonyBlok;
declare -A PLANSZA;
declare kolorPlanszy=$pomaranczowyBlok;
declare -i poczatekPaddlea;
declare -i koniecPaddlea;
declare klawisz;
declare -i czyKoniecGry;
declare -i liczbaRzedowBlokow=4;
declare -i szerokoscBloku=8;
declare -i katOdbicia; #kąt odbicia piłki: 0 - "pionowy" w górę, 1 - "pionowy" w dół, 2 - na lewo i w górę, 3 - na lewo i w dół, 4 - na prawo i w górę, 5 - na prawo i w dół
declare -i pozycjaPilkix;
declare -i pozycjaPilkiy;
declare nazwaGracza=$(whoami);
declare -i wynik;
declare kolorPaddlea=$czerwonyBlok;
declare kolorPilki=$niebieskiBlok;
declare -i iloscBlokow;
declare -i wynik1;
declare -i wynik2;
declare -i poczatekPaddlea1;
declare -i koniecPaddlea1;
declare -i poczatekPaddlea2;
declare -i koniecPaddlea2;
declare -i oczekiwanaPozycjaPaddlea; #podczas gry Cymbergaj z komputerem, jest to docelowa pozycja paddle'a komputera
declare -i wyswietlanieMiejscaSpadkuPilki=1;
declare -i grazKomputerem=1;
########################################################################

##########DEKLARACJE FUNKCJI############################################
function Breakout()
{
	katOdbicia=0;
	wynik=0;
	sleep 0.1;
	printf "\e[8;41;60t";
	sleep 0.1;
	inicjalizujPlanszeBreakout;

	while (($czyKoniecGry == 0))
	do
		aktualizujWynikBreakout;
		obslugaKlawiszyBreakout;
		if (($pozycjaPilkix == $liczbaWierszy - 1))
		then
			czyKoniecGry=1;
			break;
		fi
		ruszPilka;
	done
	sleep 0.1;
	printf "\e[8;41;76t";
	sleep 0.1;
	ekranKoncowy "B";
}

function Cymbergaj()
{
	sleep 0.1;
	printf "\e[8;45;50t";
	sleep 0.1;
	liczbaWierszy=$(($(tput lines) - 1));
	liczbaKolumn=$(tput cols);
	wynik1=0;
	wynik2=0;
	katOdbicia=$(($RANDOM % 2));
	ustawPozycjePilkiiPaddli;
	inicjalizujPlanszeCymbergaj;
	
	while (($czyKoniecGry == 0))
	do
		aktualizujWynikCymbergaj;
		obslugaKlawiszyCymbergaj;
		local kat=$katOdbicia;
		if (($pozycjaPilkix == 1 && $[$katOdbicia % 2] == 0)) #piłka leci w górę i jest tuż pod górnym paddlem
		then
			obsluzKolizjezGornymPaddlem;
		else
			ruszPilka;
		fi
		if ((($pozycjaPilkix == $liczbaWierszy - 2 || $pozycjaPilkix == 1) && $kat != $katOdbicia)) #piłka odbiła się od któregoś z paddli
		then
			rysujPrzyszlaPozycjePilki;
		fi
		if (($pozycjaPilkix == 0))
		then
			((wynik1++));
			ustawPozycjePilkiiPaddli;
			katOdbicia=0;
			sleep 0.3;
		fi
		if (($pozycjaPilkix == $liczbaWierszy - 1))
		then
			((wynik2++));
			ustawPozycjePilkiiPaddli;
			katOdbicia=1;
			sleep 0.3;
		fi
		if (($wynik1 == 5 || $wynik2 == 5))	
		then
			aktualizujWynikCymbergaj;
			czyKoniecGry=1;
		fi
	done
	sleep 0.1;
	printf "\e[8;44;76t";
	sleep 0.1;
	ekranKoncowy "C"; 
}

function aktualizujWynikCymbergaj()
{
	printf "\e[$[$liczbaWierszy + 1];0f"
	printf " ";
	printf " ";
	printf "\e[$[$liczbaWierszy + 1];3f\e[38;5;214m\e[100mGracz1: $wynik1\r";
	printf "\e[$[$liczbaWierszy + 1];$[$liczbaKolumn - 10]fGracz2: $wynik2\r";
	printf "\e[0m";
}

function aktualizujWynikBreakout()
{
	printf "\e[$[$liczbaWierszy + 1];0f"
	printf " ";
	printf " ";
	printf "\e[$[$liczbaWierszy + 1];3f\e[38;5;214m\e[100mNazwa gracza: $nazwaGracza\r";
	printf "\e[$[$liczbaWierszy + 1];$[$liczbaKolumn - 19]fIlość punktów: $wynik\r";
	printf "\e[0m";
}

function ustawPozycjePilkiiPaddli()
{
	pozycjaPilkix=$(($liczbaWierszy / 2));
	pozycjaPilkiy=$(($liczbaKolumn / 2 - 1));
	poczatekPaddlea1=$(($liczbaKolumn / 2 - $szerokoscPaddlea / 2));
	koniecPaddlea1=$(($liczbaKolumn / 2 + szerokoscPaddlea / 2));
	poczatekPaddlea2=$(($liczbaKolumn / 2 - $szerokoscPaddlea / 2));
	koniecPaddlea2=$(($liczbaKolumn / 2 + szerokoscPaddlea / 2));
	((koniecPaddlea1--));
	((koniecPaddlea2--));
	oczekiwanaPozycjaPaddlea=$poczatekPaddlea2;
	for ((i = 2; i < $liczbaKolumn - 1; i++))
	do
		PLANSZA[0,$i]=" ";
		PLANSZA[$[$liczbaWierszy - 1],$i]=" ";
	done
	for ((i = poczatekPaddlea1; i <= koniecPaddlea1; i++))
	do
		PLANSZA[$(($liczbaWierszy - 1)),$i]=$kolorPaddlea;
	done
	for ((i = poczatekPaddlea2; i <= koniecPaddlea2; i++))
	do
		PLANSZA[0,$i]=$kolorPaddlea;
	done
	printf "\e[1;3f";
	for ((i = 2; i < $liczbaKolumn - 2; i++))
	do
		printf "${PLANSZA[0,$i]}";
	done
	printf "\e[$liczbaWierszy;3f";
	for ((i = 2; i < $liczbaKolumn - 2; i++))
	do
		printf "${PLANSZA[$[$liczbaWierszy - 1],$i]}";
	done
}

function stworz8() #najpierw inicjalizujemy ósemkę, a potem wycinamy niektóre jej części, żeby otrzymać litery
{
	local g=$1;
	local d=$2;
	local a=$3;
	for ((i = $g; i <= $d; i++))
	do
		PLANSZA[$i,$a]=$kolorPlanszy;
	done
	((a++));
	PLANSZA[$g,$a]=$kolorPlanszy;
	PLANSZA[$(($d - 2)),$a]=$kolorPlanszy;
	PLANSZA[$d,$a]=$kolorPlanszy;
	((a++));
	PLANSZA[$g,$a]=$kolorPlanszy;
	PLANSZA[$(($g + 1)),$a]=$kolorPlanszy;
	PLANSZA[$(($g + 2)),$a]=$kolorPlanszy;
	PLANSZA[$(($d - 1)),$a]=$kolorPlanszy;
	PLANSZA[$d,$a]=$kolorPlanszy;
}

function ekranKoncowy()
{
	liczbaWierszy=$(($(tput lines) - 1));
	liczbaKolumn=$(tput cols);
	printf "\r";
	local g=16; #góra napisu
	local d=$(($g + 4)); #dół napisu
	local a=26; #aktualny numer kolumny
	#obramowanie
	for ((i = 0; i < $liczbaKolumn; i++))
	do
		PLANSZA[0,$i]=$kolorPlanszy;
		PLANSZA[$liczbaWierszy,$i]=$kolorPlanszy;
	done
	
	for ((i = 1; i < $liczbaWierszy; i++))
	do
		PLANSZA[$i,0]=$kolorPlanszy;
		PLANSZA[$i,1]=$kolorPlanszy;
		for ((j = 2; j < $liczbaKolumn - 1; j++))
		do
			PLANSZA[$i,$j]=" ";
		done
		PLANSZA[$i,$(($liczbaKolumn - 1))]=$kolorPlanszy;
		PLANSZA[$i,$(($liczbaKolumn - 2))]=$kolorPlanszy;
	done
	#napis "KONIEC GRY!"
	#K
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$g,$a]=" ";
	PLANSZA[$d,$a]=" ";
	((a++));
	PLANSZA[$(($g + 2)),$a]=" ";
	((a += 2));
	#O
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$(($g + 2)),$a]=" ";
	((a++));
	((a += 2));
	#N
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$g,$a]=" ";
	PLANSZA[$d,$a]=" ";
	((a++));
	((a += 2));
	#I
	PLANSZA[$d,$a]=$kolorPlanszy;
	PLANSZA[$(($d - 1)),$a]=$kolorPlanszy;
	PLANSZA[$(($d - 2)),$a]=$kolorPlanszy;
	PLANSZA[$(($d - 3)),$a]=$kolorPlanszy;
	PLANSZA[$(($d - 4)),$a]=$kolorPlanszy;
	((a += 2));
	#E
	stworz8 $g $d $a;
	((a += 2));
	PLANSZA[$(($g + 1)),$a]=" ";
	PLANSZA[$(($d - 1)),$a]=" ";
	((a += 2));
	#C
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$(($d - 2)),$a]=" ";
	((a++));
	PLANSZA[$(($d - 1)),$a]=" ";
	PLANSZA[$(($d - 2)),$a]=" ";
	PLANSZA[$(($d - 3)),$a]=" ";
	#\n
	a=30;
	g=$(($d + 3));
	d=$(($g + 4));
	#G
	stworz8 $g $d $a;
	((a += 2));
	PLANSZA[$(($g + 1)),$a]=" ";
	((a += 2));
	#R
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$d,$a]=" ";
	((a++));
	PLANSZA[$g,$a]=" ";
	PLANSZA[$(($g + 2)),$a]=" ";
	((a += 2));
	#Y
	PLANSZA[$g,$a]=$kolorPlanszy;
	PLANSZA[$(($g + 1)),$a]=$kolorPlanszy;
	((a++));
	PLANSZA[$d,$a]=$kolorPlanszy;
	PLANSZA[$(($d - 1)),$a]=$kolorPlanszy;
	PLANSZA[$(($d - 2)),$a]=$kolorPlanszy;
	((a++));
	PLANSZA[$g,$a]=$kolorPlanszy;
	PLANSZA[$(($g + 1)),$a]=$kolorPlanszy;
	((a += 2));
	#!
	PLANSZA[$g,$a]=$kolorPlanszy;
	PLANSZA[$(($g + 1)),$a]=$kolorPlanszy;
	PLANSZA[$(($g + 2)),$a]=$kolorPlanszy;
	PLANSZA[$(($g + 4)),$a]=$kolorPlanszy;
	
	printf "\e[0;0f";
	rysujPlansze;
	if [ "$1" == "B" ]
	then
		printf "\e[30;27f\e[38;5;214m\e[100mTwój wynik: ${wynik}pkt.";
	else
		printf "\e[30;27f\e[38;5;214m\e[100mGracz1: ${wynik1}   Gracz2: ${wynik2}";
	fi
	printf "\e[31;27f\e[38;5;214m\e[100m1) Menu główne.";
	printf "\e[32;27f\e[38;5;214m\e[100m2) Wyjście z gry.";
	printf "\e[0m";
	printf "\e[0;0f";
	while [ 1 ]
	do
		read -rsn1 -d "" klawisz;
		if [ "$klawisz" == "1" ]
		then
			ekranPoczatkowy;
			return;
		fi
		if [ "$klawisz" == "2" ]
		then
			return;
		fi
	done
}

function ekranPoczatkowy()
{
	local g=16; #góra napisu
	local d=$(($g + 4)); #dół napisu
	local a=23; #aktualny numer kolumny
	#obramowanie
	for ((i = 0; i < $liczbaKolumn; i++))
	do
		PLANSZA[0,$i]=$kolorPlanszy;
		PLANSZA[$liczbaWierszy,$i]=$kolorPlanszy;
	done
	
	for ((i = 1; i < $liczbaWierszy; i++))
	do
		PLANSZA[$i,0]=$kolorPlanszy;
		PLANSZA[$i,1]=$kolorPlanszy;
		for ((j = 2; j < $liczbaKolumn - 1; j++))
		do
			PLANSZA[$i,$j]=" ";
		done
		PLANSZA[$i,$(($liczbaKolumn - 1))]=$kolorPlanszy;
		PLANSZA[$i,$(($liczbaKolumn - 2))]=$kolorPlanszy;
	done
	#napis "BREAKOUT"
	#B
	stworz8 $g $d $a;
	((a += 2));
	PLANSZA[$(($g + 2)),$a]=" ";
	((a += 2));
	#R
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$d,$a]=" ";
	((a++));
	PLANSZA[$g,$a]=" ";
	PLANSZA[$(($g + 2)),$a]=" ";
	((a += 2));
	#E
	stworz8 $g $d $a;
	((a += 2));
	PLANSZA[$(($g + 1)),$a]=" ";
	PLANSZA[$(($d - 1)),$a]=" ";
	((a += 2));
	#A
	stworz8 $g $d $a;
	PLANSZA[$g,$a]=" ";
	((a++));
	PLANSZA[$d,$a]=" ";
	((a++));
	PLANSZA[$g,$a]=" ";
	((a += 2));
	#K
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$g,$a]=" ";
	PLANSZA[$d,$a]=" ";
	((a++));
	PLANSZA[$(($g + 2)),$a]=" ";
	((a += 2));
	#O
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$(($g + 2)),$a]=" ";
	((a++));
	((a += 2));
	#U
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$g,$a]=" ";
	PLANSZA[$(($g + 2)),$a]=" ";
	((a++));
	((a += 2));
	#T
	stworz8 $g $d $a;
	PLANSZA[$d,$a]=" ";
	PLANSZA[$(($d - 1)),$a]=" ";
	PLANSZA[$(($d - 2)),$a]=" ";
	PLANSZA[$(($d - 3)),$a]=" ";
	((a++));
	PLANSZA[$(($g + 1)),$a]=$kolorPlanszy;
	PLANSZA[$(($g + 3)),$a]=$kolorPlanszy;
	((a++));
	PLANSZA[$d,$a]=" ";
	PLANSZA[$(($d - 1)),$a]=" ";
	PLANSZA[$(($d - 2)),$a]=" ";
	PLANSZA[$(($d - 3)),$a]=" ";
	
	rysujPlansze;
	printf "\e[$(($d + 3));24f\e[38;5;214m\e[100mWybierz tryb gry:";
	printf "\e[$(($d + 4));24f1) Breakout.\n";
	printf "\e[$(($d + 5));24f2) Cymbergaj.\n";
	printf "\e[$(($d + 6));24f3) Opcje.\n";
	printf "\e[$(($d + 7));24f4) Wyjście z gry.\n";
	printf "\e[0m";
	printf "\e[0;0f";
	
	while [ 1 ]
	do
		czyKoniecGry=0;
		read -rsn1 -d "" klawisz;
		if [ "$klawisz" == "1" ]
		then
			clear;
			Breakout;
			return;
		fi
		if [ "$klawisz" == "2" ]
		then
			clear;
			Cymbergaj;
			return;
		fi
		if [ "$klawisz" == "3" ]
		then
			clear;
			Opcje;
			return;
		fi
		if [ "$klawisz" == "4" ]
		then
			return;
		fi
	done
}

function Opcje()
{
	local g=14; #góra napisu
	local d=$(($g + 4)); #dół napisu
	local a=26; #aktualny numer kolumny
	#obramowanie
	for ((i = 0; i < $liczbaKolumn; i++))
	do
		PLANSZA[0,$i]=$kolorPlanszy;
		PLANSZA[$liczbaWierszy,$i]=$kolorPlanszy;
	done
	
	for ((i = 1; i < $liczbaWierszy; i++))
	do
		PLANSZA[$i,0]=$kolorPlanszy;
		PLANSZA[$i,1]=$kolorPlanszy;
		for ((j = 2; j < $liczbaKolumn - 1; j++))
		do
			PLANSZA[$i,$j]=" ";
		done
		PLANSZA[$i,$(($liczbaKolumn - 1))]=$kolorPlanszy;
		PLANSZA[$i,$(($liczbaKolumn - 2))]=$kolorPlanszy;
	done
	#napis "OPCJE"
	#O
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$(($g + 2)),$a]=" ";
	((a++));
	((a += 2));
	#P
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$d,$a]=" ";
	((a++));
	PLANSZA[$d,$a]=" ";
	PLANSZA[$(($d - 1)),$a]=" ";
	((a += 2));
	#C
	stworz8 $g $d $a;
	((a++));
	PLANSZA[$(($d - 2)),$a]=" ";
	((a++));
	PLANSZA[$(($d - 1)),$a]=" ";
	PLANSZA[$(($d - 2)),$a]=" ";
	PLANSZA[$(($d - 3)),$a]=" ";
	((a += 2));
	#J
	stworz8 $g $d $a;
	PLANSZA[$(($g + 1)),$a]=" ";
	PLANSZA[$(($g + 2)),$a]=" ";
	((a++));
	PLANSZA[$(($g + 2)),$a]=" ";
	((a++));
	((a += 2));
	#E
	stworz8 $g $d $a;
	((a += 2));
	PLANSZA[$(($g + 1)),$a]=" ";
	PLANSZA[$(($d - 1)),$a]=" ";
	((a += 2));
	#:
	PLANSZA[$(($d - 2)),$a]=$kolorPlanszy;
	PLANSZA[$d,$a]=$kolorPlanszy;
	
	rysujPlansze;
	printf "\e[$(($d + 3));27f\e[38;5;214m\e[100m1) Wyświetlanie miejsca spadku piłki:"
	if (($wyswietlanieMiejscaSpadkuPilki == 1))
	then
		printf "$zielonyBlok\n";
	else
		printf "$czerwonyBlok\n";
	fi
	printf "\e[$(($d + 4));27f\e[38;5;214m\e[100m2) Gra z komputerem:";
	if (($grazKomputerem == 1))
	then
		printf "$zielonyBlok\n";
	else
		printf "$czerwonyBlok\n";
	fi
	printf "\e[$(($d + 5));27f\e[38;5;214m\e[100m3) Szerokość Paddle'a (+/-):";
	for ((i = 0; i < $szerokoscPaddlea; i++))
	do
		printf "$kolorPaddlea";
	done
	printf "\n";
	printf "\e[$(($d + 6));27f\e[38;5;214m\e[100m4) Menu główne.\n";
	printf "\e[0m";
	printf "\e[0;0f";
	
	while [ 1 ]
	do
		read -rsn1 -d "" klawisz;
		if [ "$klawisz" == "1" ]
		then
			if (($wyswietlanieMiejscaSpadkuPilki == 1))
			then
				printf "\e[$(($d + 3));64f\e[38;5;214m\e[100m$czerwonyBlok";
				wyswietlanieMiejscaSpadkuPilki=0;
			else
				printf "\e[$(($d + 3));64f\e[38;5;214m\e[100m$zielonyBlok";
				wyswietlanieMiejscaSpadkuPilki=1;
			fi
		fi
		if [ "$klawisz" == "2" ]
		then
			if (($grazKomputerem == 1))
			then
				printf "\e[$(($d + 4));47f\e[38;5;214m\e[100m$czerwonyBlok";
				grazKomputerem=0;
			else
				printf "\e[$(($d + 4));47f\e[38;5;214m\e[100m$zielonyBlok";
				grazKomputerem=1;
			fi
		fi
		if [ "$klawisz" == "+" ] && (($szerokoscPaddlea < 20))
		then
			printf "\e[$(($d + 5));$[55 + $szerokoscPaddlea]f$kolorPaddlea$kolorPaddlea";
			((szerokoscPaddlea += 2));
		fi
		if [ "$klawisz" == "-" ] && (($szerokoscPaddlea > 4))
		then
			((szerokoscPaddlea -= 2));
			printf "\e[$(($d + 5));$[55 + $szerokoscPaddlea]f$czarnyBlok$czarnyBlok";
		fi
		if [ "$klawisz" == "4" ]
		then
			ekranPoczatkowy;
			return;
		fi
	done
}

function inicjalizujPlanszeCymbergaj()
{
	for ((i = 0; i < $liczbaWierszy; i++))
	do
		PLANSZA[$i,0]=$kolorPlanszy;
		PLANSZA[$i,1]=$kolorPlanszy;
		for ((j = 2; j < $liczbaKolumn - 1; j++))
		do
			PLANSZA[$i,$j]=" ";
		done
		PLANSZA[$i,$(($liczbaKolumn - 1))]=$kolorPlanszy;
		PLANSZA[$i,$(($liczbaKolumn - 2))]=$kolorPlanszy;
	done
	
	for ((i = poczatekPaddlea1; i <= koniecPaddlea1; i++))
	do
		PLANSZA[$(($liczbaWierszy - 1)),$i]=$kolorPaddlea;
	done
	
	for ((i = poczatekPaddlea2; i <= koniecPaddlea2; i++))
	do
		PLANSZA[0,$i]=$kolorPaddlea;
	done
	rysujPlansze;
}

function poziom1()
{
	local aktualnyBlok=$kolorBloku1;
	for ((i = 1; i < 2 * $liczbaRzedowBlokow; i += 2))
	do
		for ((j = 2; j < $liczbaKolumn - $szerokoscBloku; j += $szerokoscBloku))
		do
			for ((k = $j; k < $j + $szerokoscBloku; k++))
			do
				PLANSZA[$i,$k]=$aktualnyBlok;
				PLANSZA[$(($i + 1)),$k]=$aktualnyBlok;
			done
			if [ "$aktualnyBlok" == "$kolorBloku1" ]
			then
				aktualnyBlok=$kolorBloku2;
			else
				aktualnyBlok=$kolorBloku1;
			fi
			((iloscBlokow++));
		done
	done
}

function poziom2()
{
	local aktualnyBlok=$kolorBloku1;
	for ((i = 1; i < 5; i += 2))
	do
		for ((j = 2; j < $liczbaKolumn - $szerokoscBloku; j += $szerokoscBloku))
		do
			for ((k = $j; k < $j + $szerokoscBloku; k++))
			do
				PLANSZA[$i,$k]=$aktualnyBlok;
				PLANSZA[$(($i + 1)),$k]=$aktualnyBlok;
			done
			if [ "$aktualnyBlok" == "$kolorBloku1" ]
			then
				aktualnyBlok=$kolorBloku2;
			else
				aktualnyBlok=$kolorBloku1;
			fi
			((iloscBlokow++));
		done
	done
	for ((j = 2; j < 2 + 3 * $szerokoscBloku; j += $szerokoscBloku))
	do
		for ((k = $j; k < $j + $szerokoscBloku; k++))
		do
			PLANSZA[5,$k]=$aktualnyBlok;
			PLANSZA[6,$k]=$aktualnyBlok;
		done
		if [ "$aktualnyBlok" == "$kolorBloku1" ]
		then
			aktualnyBlok=$kolorBloku2;
		else
			aktualnyBlok=$kolorBloku1;
		fi
		((iloscBlokow++));
	done
	if [ "$aktualnyBlok" == "$kolorBloku1" ]
	then
		aktualnyBlok=$kolorBloku2;
	else
		aktualnyBlok=$kolorBloku1;
	fi
	for ((j = 2 + 4 * $szerokoscBloku; j < $liczbaKolumn - $szerokoscBloku; j += $szerokoscBloku))
	do
		for ((k = $j; k < $j + $szerokoscBloku; k++))
		do
			PLANSZA[5,$k]=$aktualnyBlok;
			PLANSZA[6,$k]=$aktualnyBlok;
		done
		if [ "$aktualnyBlok" == "$kolorBloku1" ]
		then
			aktualnyBlok=$kolorBloku2;
		else
			aktualnyBlok=$kolorBloku1;
		fi
		((iloscBlokow++));
	done
	
	for ((j = 2; j < 2 + 2 * $szerokoscBloku; j += $szerokoscBloku))
	do
		for ((k = $j; k < $j + $szerokoscBloku; k++))
		do
			PLANSZA[7,$k]=$aktualnyBlok;
			PLANSZA[8,$k]=$aktualnyBlok;
		done
		if [ "$aktualnyBlok" == "$kolorBloku1" ]
		then
			aktualnyBlok=$kolorBloku2;
		else
			aktualnyBlok=$kolorBloku1;
		fi
		((iloscBlokow++));
	done
	if [ "$aktualnyBlok" == "$kolorBloku1" ]
	then
		aktualnyBlok=$kolorBloku2;
	else
		aktualnyBlok=$kolorBloku1;
	fi
	for ((j = 2 + 5 * $szerokoscBloku; j < $liczbaKolumn - $szerokoscBloku; j += $szerokoscBloku))
	do
		for ((k = $j; k < $j + $szerokoscBloku; k++))
		do
			PLANSZA[7,$k]=$aktualnyBlok;
			PLANSZA[8,$k]=$aktualnyBlok;
		done
		if [ "$aktualnyBlok" == "$kolorBloku1" ]
		then
			aktualnyBlok=$kolorBloku2;
		else
			aktualnyBlok=$kolorBloku1;
		fi
		((iloscBlokow++));
	done
	for ((j = 2; j < 2 + $szerokoscBloku; j += $szerokoscBloku))
	do
		for ((k = $j; k < $j + $szerokoscBloku; k++))
		do
			PLANSZA[9,$k]=$aktualnyBlok;
			PLANSZA[10,$k]=$aktualnyBlok;
		done
		if [ "$aktualnyBlok" == "$kolorBloku1" ]
		then
			aktualnyBlok=$kolorBloku2;
		else
			aktualnyBlok=$kolorBloku1;
		fi
		((iloscBlokow++));
	done
	if [ "$aktualnyBlok" == "$kolorBloku1" ]
	then
		aktualnyBlok=$kolorBloku2;
	else
		aktualnyBlok=$kolorBloku1;
	fi
	for ((j = 2 + 6 * $szerokoscBloku; j < $liczbaKolumn - $szerokoscBloku; j += $szerokoscBloku))
	do
		for ((k = $j; k < $j + $szerokoscBloku; k++))
		do
			PLANSZA[9,$k]=$aktualnyBlok;
			PLANSZA[10,$k]=$aktualnyBlok;
		done
		if [ "$aktualnyBlok" == "$kolorBloku1" ]
		then
			aktualnyBlok=$kolorBloku2;
		else
			aktualnyBlok=$kolorBloku1;
		fi
		((iloscBlokow++));
	done
}

function inicjalizujPlanszeBreakout()
{
	liczbaWierszy=$(($(tput lines) - 1));
	liczbaKolumn=$(tput cols);
	poczatekPaddlea=$(($liczbaKolumn / 2 - $szerokoscPaddlea / 2));
	koniecPaddlea=$(($liczbaKolumn / 2 + szerokoscPaddlea / 2));
	((koniecPaddlea--));
	for ((i = 0; i < $liczbaKolumn; i++))
	do
		PLANSZA[0,$i]=$kolorPlanszy;
		PLANSZA[$liczbaWierszy,$i]=" ";
	done
	
	for ((i = 1; i < $liczbaWierszy; i++))
	do
		PLANSZA[$i,0]=$kolorPlanszy;
		PLANSZA[$i,1]=$kolorPlanszy;
		for ((j = 2; j < $liczbaKolumn - 1; j++))
		do
			PLANSZA[$i,$j]=" ";
		done
		PLANSZA[$i,$(($liczbaKolumn - 1))]=$kolorPlanszy;
		PLANSZA[$i,$(($liczbaKolumn - 2))]=$kolorPlanszy;
	done
	
	for ((i = poczatekPaddlea; i <= koniecPaddlea; i++))
	do
		PLANSZA[$(($liczbaWierszy - 1)),$i]=$kolorPaddlea;
	done
	
	#DODANIE BLOKÓW
	local rand=$(($RANDOM % 2));
	if (($rand == 0))
	then
		poziom1;
	fi
	if (($rand == 1))
	then
		poziom2;
	fi
	
	pozycjaPilkix=$(($liczbaWierszy - 2));
	pozycjaPilkiy=$(($liczbaKolumn / 2 - 1));
	rysujPlansze;
}

function rysujPlansze()
{
	printf "\e[0;0f";
	for ((i = 0; i <= $liczbaWierszy; i++))
	do
		for ((j = 0; j < $liczbaKolumn; j++))
		do
			printf "${PLANSZA[$i,$j]}";
		done
	done
}

function ruszPaddlea()
{
	local wiersz=$1;
	local poczatek=$2;
	local koniec=$3;
	local doSkasowania=$4;
	PLANSZA[$wiersz,$poczatek]=$kolorPaddlea;
	PLANSZA[$wiersz,$koniec]=$kolorPaddlea;
	PLANSZA[$wiersz,$doSkasowania]=" ";
	
	#####Przesunięcie paddla na ekranie
	printf "\e[$[$wiersz + 1];$[$poczatek + 1]f";
	printf "$kolorPaddlea";
	printf "\e[$[$wiersz + 1];$[$koniec + 1]f";
	printf "$kolorPaddlea";
	printf "\e[$[$wiersz + 1];$[doSkasowania + 1]f";
	printf " ";
}

function ruchKomputera()
{
	if (($poczatekPaddlea2 < $oczekiwanaPozycjaPaddlea))
	then
		((poczatekPaddlea2++));
		((koniecPaddlea2++));
		ruszPaddlea 0 $poczatekPaddlea2 $koniecPaddlea2 $[$poczatekPaddlea2 - 1];
	fi
	if (($poczatekPaddlea2 > $oczekiwanaPozycjaPaddlea))
	then
		((poczatekPaddlea2--));
		((koniecPaddlea2--));
		ruszPaddlea 0 $poczatekPaddlea2 $koniecPaddlea2 $[$koniecPaddlea2 + 1];
	fi
}

function obslugaKlawiszyCymbergaj()
{
	klawisz="";
	read -rsn1 -t 0.02 -d "" klawisz;
	if [ "$klawisz" == "d" ] #ruch paddlea1 w prawo
	then
		if (($koniecPaddlea1 < $liczbaKolumn - 3))
		then
			((poczatekPaddlea1++));
			((koniecPaddlea1++));
			ruszPaddlea $[$liczbaWierszy - 1] $poczatekPaddlea1 $koniecPaddlea1 $[$poczatekPaddlea1 - 1];
		fi
	fi
	
	if [ "$klawisz" == "a" ] #ruch paddlea1 w lewo
	then
		if (($poczatekPaddlea1 > 2))
		then
			((poczatekPaddlea1--));
			((koniecPaddlea1--));
			ruszPaddlea $[$liczbaWierszy - 1] $poczatekPaddlea1 $koniecPaddlea1 $[$koniecPaddlea1 + 1];
		fi
	fi
	
	if (($grazKomputerem == 0))
	then
		if [ "$klawisz" == "l" ] #ruch paddlea2 w prawo
		then
			if (($koniecPaddlea2 < $liczbaKolumn - 3))
			then
				((poczatekPaddlea2++));
				((koniecPaddlea2++));
				ruszPaddlea 0 $poczatekPaddlea2 $koniecPaddlea2 $[$poczatekPaddlea2 - 1];
			fi
		fi
		
		if [ "$klawisz" == "j" ] #ruch paddlea2 w lewo
		then
			if (($poczatekPaddlea2 > 2))
			then
				((poczatekPaddlea2--));
				((koniecPaddlea2--));
				ruszPaddlea 0 $poczatekPaddlea2 $koniecPaddlea2 $[$koniecPaddlea2 + 1];
			fi
		fi
	else
		ruchKomputera;
	fi
	
	if [ "$klawisz" == "q" ] #koniec gry
	then
		czyKoniecGry=1;
	fi
}

function obslugaKlawiszyBreakout()
{
	klawisz="";
	read -rsn1 -t 0.06 -d "" klawisz;
	if [ "$klawisz" == "d" ] #strzalka w prawo
	then
		if (($koniecPaddlea < $liczbaKolumn - 3))
		then
			((poczatekPaddlea++));
			((koniecPaddlea++));
			ruszPaddlea $[$liczbaWierszy - 1] $poczatekPaddlea $koniecPaddlea $[$poczatekPaddlea - 1];
		fi
	fi
	
	if [ "$klawisz" == "a" ] #strzalka w lewo
	then
		if (($poczatekPaddlea > 2))
		then
			((poczatekPaddlea--));
			((koniecPaddlea--));
			ruszPaddlea $[$liczbaWierszy - 1] $poczatekPaddlea $koniecPaddlea $[$koniecPaddlea + 1];
		fi
	fi
	
	if [ "$klawisz" == "q" ] #koniec gry
	then
		czyKoniecGry=1;
	fi
}

function ustawOczekwanaPozycjePaddlea()
{
	local y=$1;
	if (($liczbaKolumn - 2 - $szerokoscPaddlea <= $y)) #mamy tylko dwie opcje odbicia: prosto i w prawo
	then
		if (($RANDOM % 2 == 0)) #prosto
		then
			oczekiwanaPozycjaPaddlea=$(($liczbakolumn - 2 - $szerokoscPaddlea));
		else #w prawo
			oczekiwanaPozycjaPaddlea=$(($pozycjaPilkiy - $szerokoscPaddlea + 1));
		fi
		return;
	fi
	if (($y <= $szerokoscPaddlea + 1)) #mamy tylko dwie opcje odbicia: prosto i w lewo
	then
		if (($RANDOM % 2 == 0)) #prosto
		then
			oczekiwanaPozycjaPaddlea=2;
		else #w lewo
			oczekiwanaPozycjaPaddlea=$(($y + 1));
		fi
		return;
	fi
	#mamy trzy możliwości odbicia
	local rand=$(($RANDOM % 3));
	if (($rand == 0)) #prosto
	then
		oczekiwanaPozycjaPaddlea=$(($y - $(($szerokoscPaddlea / 2)) + 1));
	fi
	if (($rand == 1)) #w lewo
	then
		oczekiwanaPozycjaPaddlea=$(($y + 1));
	fi
	if (($rand == 2)) #w prawo
	then
		oczekiwanaPozycjaPaddlea=$(($y - $szerokoscPaddlea + 1));
	fi
}

function rysujPilke()
{
	if (($wyswietlanieMiejscaSpadkuPilki == 0))
	then
		return;
	fi
	printf "\e[$[$1 - 1];$[$2 + 1]f"
	printf "$niebieskoFioletowyBlok";
	printf "$niebieskoFioletowyBlok";
}

function rysujPrzyszlaPozycjePilki()
{
	local x=$pozycjaPilkix;
	local y=$pozycjaPilkiy;
	local kat=$katOdbicia;
	if (($kat == 1))
	then
		rysujPilke $liczbaWierszy $y;
		return;
	fi
	if (($kat == 0))
	then
		rysujPilke 3 $y;
		ustawOczekwanaPozycjePaddlea $y;
		return;
	fi
	
	while [ 1 ]
	do
		if (($kat == 3))
		then
			((x++));
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y - 1))]}" != " " ] || [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] #kolizja
			then
				#zderzenie ze ścianą
				if (($y - 1 <= 1)) 
				then
					((x--)); #nie zmieniamy x; wcześniej go zwiększyliśmy
					kat=5;
					continue;
				fi
				#zderzenie z blokiem / blokami na dole
				if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
				then
					return;
				fi
				#zderzenie z blokiem od jego prawego boku
				if [ "${PLANSZA[$(($x - 1)),$(($y - 1))]}" != " " ]
				then
					((x--));
					kat=5;
					continue; 
				fi
				#zderzenie z blokiem "rogami"
				if [ "${PLANSZA[$x,$(($y - 1))]}" != " " ]
				then
					return;
				fi
			fi
			#nie ma kolizji; już zwiększyliśmy x
			((y--));
		fi
		if (($kat == 5))
		then
			((x++));
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x - 1)),$(($y + 2))]}" != " " ] || [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] #kolizja
			then
				#zderzenie ze ścianą
				if (($y + 2 >= $liczbaKolumn - 2)) 
				then
					((x--));
					kat=3;
					continue;
				fi
				#zderzenie z blokiem / blokami na dole
				if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
				then
					return;
				fi
				#zderzenie z blokiem od jego lewego boku
				if [ "${PLANSZA[$(($x - 1)),$(($y + 2))]}" != " " ]
				then
					((x--));
					kat=3;
					continue; 
				fi
				#zderzenie z blokiem "rogami"
				if [ "${PLANSZA[$x,$(($y + 2))]}" != " " ]
				then
					return;
				fi
			fi
			#nie ma kolizji; już zwiększyliśmy x
			((y++));
		fi
		if (($kat == 2))
		then
			((x--));
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y - 1))]}" != " " ] || [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] #kolizja
			then
				#zderzenie ze ścianą
				if (($y - 1 <= 1)) 
				then
					((x++));
					kat=4;
					continue;
				fi
			fi
			#nie ma kolizji
			((y--));
		fi
		if (($kat == 4))
		then
			((x--));
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y + 2))]}" != " " ] || [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] #kolizja
			then
				#zderzenie ze ścianą
				if (($y + 2 >= $liczbaKolumn - 2)) 
				then
					((x++));
					kat=2;
					continue;
				fi
			fi
			#nie ma kolizji
			((y++));
		fi
		
		if (($x == $liczbaWierszy - 2))
		then
			rysujPilke $liczbaWierszy $y;
			return;
		fi
		if (($x == 1))
		then
			rysujPilke 3 $y;
			ustawOczekwanaPozycjePaddlea $y;
			return;
		fi
	done
}

function usunBlok()
{
	local x=$1;
	local j=$2;
	PLANSZA[$x,$j]=" ";
	printf "\e[${x};$[$j + 1]f";
	printf "${PLANSZA[$x,$j]}";
	printf "\r";
	PLANSZA[$(($x - 1)),$j]=" ";
	((x++));
	printf "\e[${x};$[$j + 1]f";
	printf "${PLANSZA[$(($x - 1)),$j]}";
	printf "\r";
}

function usunCalyBlok()
{
	local x=$1;
	local y=$2;
	if ((x % 2 == 1))
	then
		((x++));
	fi
	for ((j = $y; $(($j - 2 + $szerokoscBloku)) % $szerokoscBloku != $(($szerokoscBloku - 1)) || $j == $y; j--))
	do
		usunBlok $x $j;
	done
	for ((j = $y; $(($j + 1)) % $szerokoscBloku != 3 || $j == $y; j++))
	do
		usunBlok $x $j;
	done
	((wynik += 10));
	aktualizujWynikBreakout;
	((iloscBlokow--));
	if (($iloscBlokow == 0))
	then
		czyKoniecGry=1;
	fi
}

function ruszPilke()
{
	local x1=$1;
	local y1=$2;
	local x2=$3;
	local y2=$4;
	printf "\e[${x1};$[$y1 + 1]f";
	printf " ";
	printf " ";
	printf "\e[${x2};$[$y2 + 1]f";
	printf "$kolorPilki";
	printf "$kolorPilki";
}

function ruszPilka()
{
	local x=$pozycjaPilkix; #żeby nie pisać cały czas długiego wyrażenia
	local y=$pozycjaPilkiy;
########################################################################
	if (($katOdbicia == 0))
	then
		((x--));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]  #góra planszy, albo blok; drugi warunek jest dlatego, że piłka składa się z dwóch bloków
		then
			katOdbicia=1; #pozycji piłki w tym momencie nie zmieniamy!; zmieniamy tylko kierunek
			rysujPrzyszlaPozycjePilki;
			if (($x < 2)) #nie chcemy usuwać góry planszy
			then
				return;
			fi
			
			if [ "${PLANSZA[$x,$y]}" != " " ] #usunięcie opcjonalnego bloku "z lewej" i wypisanie - pierwszy warunek
			then
				usunCalyBlok $x $y;
			fi
			
			if [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] #usunięcie opcjonalnego bloku "z prawej" i wypisanie - drugi warunek
			then
				((y++));
				usunCalyBlok $x $y;
			fi
		else #nie ma kolizji i zmieniamy pozycję piłki
			((x += 2));
			ruszPilke $x $y $[$x - 1] $y;
			((pozycjaPilkix--));
		fi
		return;
	fi
########################################################################
	if (($katOdbicia == 1))
	then
		((x++));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] #innymi słowy trafiliśmy w piłkę "paddlem"
		then
			if [ "${PLANSZA[$x,$y]}" != " " ] && [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] #w piłkę trafiliśmy "czysto", tzn. nie rogiem "paddle'a"
			then
				katOdbicia=0;
				return;
			fi
			if [ "${PLANSZA[$x,$y]}" != " " ] #w piłkę trafiliśmy prawym rogiem "paddle'a"
			then
				katOdbicia=4;
				return;
			fi
			if [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] #w piłkę trafiliśmy lewym rogiem "paddle'a"
			then
				katOdbicia=2;
				return;
			fi
		fi
		#jeśli nic nie stoi na przeszkodzie do ruchu w dół
		ruszPilke $x $y $[$x + 1] $y;
		((pozycjaPilkix++));
	fi
########################################################################
	if (($katOdbicia == 4))
	then
		((x--));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y + 2))]}" != " " ] || [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] #kolizja
		then
			#zderzenie ze ścianą
			if (($y + 2 >= $liczbaKolumn - 2)) 
			then
				katOdbicia=2;
				return;
			fi
			#zderzenie z sufitem
			if (($x == 0)) 
			then
				katOdbicia=5;
				rysujPrzyszlaPozycjePilki;
				return;
			fi
			#zderzenie z blokiem / blokami na górze
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
			then
				if [ "${PLANSZA[$x,$y]}" != " " ] 
				then
					usunCalyBlok $x $y;
				fi
				if [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
				then
					((y++));
					usunCalyBlok $x $y;
				fi
				katOdbicia=5;
				rysujPrzyszlaPozycjePilki;
				return;
			fi
			#zderzenie z blokiem od jego lewego boku
			if [ "${PLANSZA[$(($x + 1)),$(($y + 2))]}" != " " ]
			then
				((x++));
				((y += 2));
				usunCalyBlok $x $y;
				katOdbicia=2;
				return; 
			fi
			#zderzenie z blokiem "rogami"
			if [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] #tak naprawdę, tylko ta opcja w tym momencie zostaje
			then
				((y += 2));
				usunCalyBlok $x $y;
				katOdbicia=3;
				rysujPrzyszlaPozycjePilki;
				return;
			fi
		fi
		#nie ma kolizji
		((x += 2));
		ruszPilke $x $y $[$x - 1] $[$y + 1];
		((pozycjaPilkix--));
		((pozycjaPilkiy++));
		return;
	fi
########################################################################
	if (($katOdbicia == 2))
	then
		((x--));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y - 1))]}" != " " ] || [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] #kolizja
		then
			#zderzenie ze ścianą
			if (($y - 1 <= 1)) 
			then
				katOdbicia=4;
				return;
			fi
			#zderzenie z sufitem
			if (($x == 0)) 
			then
				katOdbicia=3;
				rysujPrzyszlaPozycjePilki;
				return;
			fi
			#zderzenie z blokiem / blokami na górze
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
			then
				if [ "${PLANSZA[$x,$y]}" != " " ] 
				then
					usunCalyBlok $x $y;
				fi
				if [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
				then
					((y++));
					usunCalyBlok $x $y;
				fi
				katOdbicia=3;
				rysujPrzyszlaPozycjePilki;
				return;
			fi
			#zderzenie z blokiem od jego prawego boku
			if [ "${PLANSZA[$(($x + 1)),$(($y - 1))]}" != " " ]
			then
				((x++));
				((y--));
				usunCalyBlok $x $y;
				katOdbicia=4;
				return; 
			fi
			#zderzenie z blokiem "rogami"
			if [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] #tak naprawdę, tylko ta opcja w tym momencie zostaje
			then
				((y--));
				usunCalyBlok $x $y;
				katOdbicia=5;
				rysujPrzyszlaPozycjePilki;
				return;
			fi
		fi
		#nie ma kolizji
		((x += 2));
		ruszPilke $x $y $[$x - 1] $[$y - 1];
		((pozycjaPilkix--));
		((pozycjaPilkiy--));
		return;
	fi
########################################################################
	if (($katOdbicia == 5))
	then
		((x++));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x - 1)),$(($y + 2))]}" != " " ] || [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] #kolizja
		then
			#zderzenie ze ścianą
			if (($y + 2 >= $liczbaKolumn - 2)) 
			then
				katOdbicia=3;
				return;
			fi
			#zderzenie z Paddlem
			if (($x == $liczbaWierszy - 1))
			then
				#"po skosie" i prawie "po skosie"
				if [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] && [ "${PLANSZA[$x,$y]}" == " " ]
				then
					katOdbicia=2;
					return;
				fi
				#prawy róg paddle'a
				if [ "${PLANSZA[$x,$(($y + 1))]}" != "$kolorPaddlea" ] && [ "${PLANSZA[$x,$y]}" == "$kolorPaddlea" ]
				then
					katOdbicia=4;
					return;
				fi
				if [ "${PLANSZA[$x,$y]}" == " " ]
				then
					ruszPilke $x $y $[$x + 1] $[$y + 1];# będzie koniec gry, ale rysujemy ostatni ruch
					((pozycjaPilkix++));
					((pozycjaPilkiy++));
					return;
				fi
				#w przeciwnym wypadku
				katOdbicia=0; #póki co w górę, można będzie zmodyfikować na 4
				return;
			fi
			#zderzenie z blokiem / blokami na dole
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
			then
				if [ "${PLANSZA[$x,$y]}" != " " ] 
				then
					usunCalyBlok $x $y;
				fi
				if [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
				then
					((y++));
					usunCalyBlok $x $y;
				fi
				katOdbicia=4;
				return;
			fi
			#zderzenie z blokiem od jego lewego boku
			if [ "${PLANSZA[$(($x - 1)),$(($y + 2))]}" != " " ]
			then
				((x--));
				((y += 2));
				usunCalyBlok $x $y;
				katOdbicia=3;
				return; 
			fi
			#zderzenie z blokiem "rogami"
			if [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] #tak naprawdę, tylko ta opcja w tym momencie zostaje
			then
				((y += 2));
				usunCalyBlok $x $y;
				katOdbicia=2;
				return;
			fi
		fi
		ruszPilke $x $y $[$x + 1] $[$y + 1];
		((pozycjaPilkix++));
		((pozycjaPilkiy++));
		return;
	fi
########################################################################
	if (($katOdbicia == 3))
	then
		((x++));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y - 1))]}" != " " ] || [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] #kolizja
		then
			#zderzenie ze ścianą
			if (($y - 1 <= 1)) 
			then
				katOdbicia=5;
				return;
			fi
			#zderzenie z Paddlem
			if (($x == $liczbaWierszy - 1))
			then
				#"po skosie" i prawie "po skosie"
				if [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] && [ "${PLANSZA[$x,$(($y + 1))]}" == " " ]
				then
					katOdbicia=4;
					return;
				fi
				#lewy róg paddle'a
				if [ "${PLANSZA[$x,$y]}" != "$kolorPaddlea" ] && [ "${PLANSZA[$x,$(($y + 1))]}" == "$kolorPaddlea" ]
				then
					katOdbicia=2;
					return;
				fi
				if [ "${PLANSZA[$x,$(($y + 1))]}" == " " ]
				then
					ruszPilke $x $y $[$x + 1] $[$y - 1]; #będzie koniec gry, ale wykonujemy ostatni ruch piłką
					((pozycjaPilkix++));
					((pozycjaPilkiy--));
					return;
				fi
				#w przeciwnym wypadku
				katOdbicia=0; #póki co w górę, można będzie zmodyfikować na 4
				return;
			fi
			#zderzenie z blokiem / blokami na dole
			if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
			then
				if [ "${PLANSZA[$x,$y]}" != " " ] 
				then
					usunCalyBlok $x $y;
				fi
				if [ "${PLANSZA[$x,$(($y + 1))]}" != " " ]
				then
					((y++));
					usunCalyBlok $x $y;
				fi
				katOdbicia=2;
				return;
			fi
			#zderzenie z blokiem od jego prawego boku
			if [ "${PLANSZA[$(($x - 1)),$(($y - 1))]}" != " " ]
			then
				((x--));
				((y--));
				usunCalyBlok $x $y;
				katOdbicia=5;
				return; 
			fi
			#zderzenie z blokiem "rogami"
			if [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] #tak naprawdę, tylko ta opcja w tym momencie zostaje
			then
				((y--));
				usunCalyBlok $x $y;
				katOdbicia=4;
				return;
			fi
		fi
		#nie ma kolizji
		ruszPilke $x $y $[$x + 1] $[$y - 1];
		((pozycjaPilkix++));
		((pozycjaPilkiy--));
		return;
	fi
}

function obsluzKolizjezGornymPaddlem()
{
	local x=$pozycjaPilkix;
	local y=$pozycjaPilkiy;
	if (($katOdbicia == 0))
	then
		((x--));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] #innymi słowy trafiliśmy w piłkę "paddlem"
		then
			if [ "${PLANSZA[$x,$y]}" != " " ] && [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] #w piłkę trafiliśmy "czysto", tzn. nie rogiem "paddle'a"
			then
				katOdbicia=1;
				return;
			fi
			if [ "${PLANSZA[$x,$y]}" != " " ] #w piłkę trafiliśmy prawym rogiem "paddle'a"
			then
				katOdbicia=5;
				return;
			fi
			if [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] #w piłkę trafiliśmy lewym rogiem "paddle'a"
			then
				katOdbicia=3;
				return;
			fi
		fi
		#jeśli nic nie stoi na przeszkodzie do ruchu w górę
		((x += 2));
		ruszPilke $x $y $[$x - 1] $y;
		((pozycjaPilkix--));
	fi
########################################################################
	if (($katOdbicia == 2))
	then
		((x--));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y - 1))]}" != " " ] || [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] #kolizja
		then
			#zderzenie ze ścianą
			if (($y - 1 <= 1)) 
			then
				katOdbicia=4;
				return;
			fi
			#zderzenie z Paddlem
			#"po skosie" i prawie "po skosie"
			if [ "${PLANSZA[$x,$(($y - 1))]}" != " " ] && [ "${PLANSZA[$x,$(($y + 1))]}" == " " ]
			then
				katOdbicia=5;
				return;
			fi
			#lewy róg paddle'a
			if [ "${PLANSZA[$x,$y]}" != "$kolorPaddlea" ] && [ "${PLANSZA[$x,$(($y + 1))]}" == "$kolorPaddlea" ]
			then
				katOdbicia=3;
				return;
			fi
			#w przeciwnym wypadku
			katOdbicia=1;
			return;
		fi
		((x += 2));
		ruszPilke $x $y $[$x - 1] $[$y - 1];
		((pozycjaPilkix--));
		((pozycjaPilkiy--)); 
		return;
	fi
########################################################################
	if (($katOdbicia == 4))
	then
		((x--));
		if [ "${PLANSZA[$x,$y]}" != " " ] || [ "${PLANSZA[$x,$(($y + 1))]}" != " " ] || [ "${PLANSZA[$(($x + 1)),$(($y + 2))]}" != " " ] || [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] #kolizja
		then
			#zderzenie ze ścianą
			if (($y + 2 >= $liczbaKolumn - 2)) 
			then
				katOdbicia=2;
				return;
			fi
			#zderzenie z Paddlem
			#"po skosie" i prawie "po skosie"
			if [ "${PLANSZA[$x,$(($y + 2))]}" != " " ] && [ "${PLANSZA[$x,$y]}" == " " ]
			then
				katOdbicia=3;
				return;
			fi
			#prawy róg paddle'a
			if [ "${PLANSZA[$x,$(($y + 1))]}" != "$kolorPaddlea" ] && [ "${PLANSZA[$x,$y]}" == "$kolorPaddlea" ]
			then
				katOdbicia=5;
				return;
			fi
			#w przeciwnym wypadku
			katOdbicia=1;
			return;
		fi
		((x += 2));
		ruszPilke $x $y $[$x - 1] $[$y + 1];
		((pozycjaPilkix--));
		((pozycjaPilkiy++));
		return;
	fi
}

##########WLASCIWA CZESC SKRYPTU########################################
ekranPoczatkowy;
clear;
########################################################################
